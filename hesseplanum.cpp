// hesseplanum.cpp -- box plot plane projected points -*- coding: utf-8 -*-

// Copyright (C) 2023 Seppo Ronkainen.

// hesseplanum is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or (at
// your option) any later version.

// hesseplanum is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with GNU Emacs.  If not, see <https://www.gnu.org/licenses/>.

// repository https://gitlab.com/sepron/hesseplanum

// prepare: g++ -O3 -o hesseplanum hesseplanum.cpp

#if __linux__
#include <dlfcn.h>
#include <fcntl.h>
#include <pwd.h>
#include <regex.h>
#include <spawn.h>
#include <sys/time.h>
#include <sys/wait.h>
#include <unistd.h>
#else
#include <Windows.h>
#endif

#include <float.h>
#include <math.h>
#include <regex>
#include <signal.h>
#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>
#include <tgmath.h>
#include <time.h>

struct buf {
  int b;   /* bytes capacity */
  int u;   /* used bytes */
  char *f; /* first byte address */
};

void buf_initude(struct buf *b) {
  b->b = 0;
  b->u = 0;
  b->f = 0;
}

int buf_load(struct buf *b, char const *path) {
#if __linux__
  int fd;

  fd = open(path, O_RDONLY);
  if (fd < 0) {
    fprintf(stderr, "{buf_load {path %s} open failed}\n", path);
    return 1;
  }
  b->b = lseek(fd, 0, SEEK_END);
  lseek(fd, 0, SEEK_SET);
  b->f = (char*)malloc(b->b);
  b->u = read(fd, b->f, b->b);
  close(fd);
#else
  HANDLE file_handle = CreateFileA(path, GENERIC_READ, 0, 0, OPEN_EXISTING, 0, 0);
  if (INVALID_HANDLE_VALUE == file_handle) {
    fprintf(stderr, "CreateFileA GENERIC_READ failure\n");
    return 1;
  }

  long long int byte_count = GetFileSize(file_handle, 0) + 1;
  char *head = new char[byte_count];
  char *tail = head + byte_count;
  unsigned long byte_count_gotten = 0;
  bool read_was_successful = ReadFile(file_handle, head, (DWORD)byte_count, &byte_count_gotten, 0);
  if (!read_was_successful) {
    fprintf(stderr, "ReadFile failure, expected {path %s}, was not accessible as a regular readable file\n", path);
  } else {
    b->b = b->u = byte_count_gotten;
    b->f = head;
  }
  CloseHandle(file_handle);
  file_handle = 0;
  fprintf(stderr, "{byte_count %lld}\n", byte_count);

#endif
  return 0;
}

static int __buf_load_scrutitude(char const *file, int line, struct buf *b, char const *path, char trap_on_failure) {
  if (buf_load(b, path)) {
    fprintf(stderr, "[ERRCODE B0A] {file %s} {line %d} \"scrutitude\" {path %s} \"was not accessible\"\n", file, line, path);
    if (trap_on_failure) {
#if __linux__
      raise(SIGTRAP);
#else
      DebugBreak();
#endif
    }
    return 0xbaadf00d;
  }

  return 0;
}

#define buf_load_or_warn(b, path) __buf_load_scrutitude(__FILE__, __LINE__, b, path, 0)

#define buf_load_or_trap(b, path) __buf_load_scrutitude(__FILE__, __LINE__, b, path, ~0)

struct point {
  double x_tum, y_tum, z_tum;
  double x_mm, y_mm, z_mm;
  int from_block;
  int from_line;
  int index;
  double plane_distance;
};

struct inspection {
  int point_count;
  struct point *point_arr;
};

struct pointref {
  int index;
};

void point_scrub(struct point *p) {
  p->from_block = -1;
  p->from_line = -1;
  p->x_mm = NAN;
  p->y_mm = NAN;
  p->z_mm = NAN;
  p->x_tum = NAN;
  p->y_tum = NAN;
  p->z_tum = NAN;
  p->plane_distance = 0;
}

void *base = 0;

int point_compare_plane_distance(void const *pl, void const *pr) {
  struct point *l = *((struct point**)pl);
  struct point *r = *((struct point**)pr);

  if (l->plane_distance == r->plane_distance) {
    return 0;
  } else {
    return l->plane_distance < r->plane_distance ? -1 : 1;
  }

  return 0;
}

struct inspection scan_inspection_from_buffer(struct buf *b) {
  struct inspection s;
  int const Lcap = 1000;
  int Lw; /* line writer */
  char L[Lcap + 1]; /* line buffer */
  char const *R;
  char const *c;
  int Le; /* line end */
  int line_index = 0;
  std::regex re_block;
  std::regex re_xyz;
  std::smatch mr;
  struct point *point_array = 0;
  int const POINT_ARRAY_CAP = 10000;
  int point_array_use = 0;
  struct point point = {0};
  char has_x = 0;
  char has_y = 0;
  char has_z = 0;
  int point_index = 0;
  char dim;

  point_array = (struct point*)malloc(POINT_ARRAY_CAP * sizeof(struct point));
  point_array_use = 0;
  re_block.assign(".*Block #([0-9]+).*");
  re_xyz.assign(".*([xXyYzZ]) +([0-9.-]+) +inches +\\((.+) +mm\\).*");
  point_scrub(&point);
  c = b->f;
  R = b->f + b->u;

  {
    char d = 0;
    char prev = 0;
    while (c < R) {
      if (prev == '\r' && *c == '\n') {
        d = 1;
        break;
      }
      prev = *c;
      ++c;
    }

    fprintf(stderr, "{detected_line_feed_carriage_return_text_format %d}\n", d);
  }

  c = b->f;
  R = b->f + b->u;
  while (c < R) {
    Lw = 0;
    while (c < R) {
      if (*c != '\n') {
        if (Lw < Lcap) {
          L[Lw] = *c;
          ++Lw;
          L[Lw] = 0;
        }
      } else {
        break;
      }
      ++c;
    }
    Le = Lw - 1;
    while (0 < Le && L[Le] == '\r') {
      L[Le] = 0;
      --Le;
    }

    if (0 < Le) {
      std::string line(L);

      if (has_x && has_y && has_z) {
        if (POINT_ARRAY_CAP <= point_array_use) {
          fprintf(stderr, "POINT_ARRAY_CAP <= point_array_use\n");
        } else {
          point.index = point_index;
          point_array[point_array_use] = point;
          ++point_index;
          ++point_array_use;
        }
        point_scrub(&point);
        has_x = 0;
        has_y = 0;
        has_z = 0;
      }

      if (std::regex_match(line, mr, re_block)) {
        point.from_block = atoi(mr[1].str().c_str());
      }

      if (std::regex_match(line, mr, re_xyz)) {
        dim = mr[1].str()[0];
        double tum = atof(mr[2].str().c_str());
        double mm = atof(mr[3].str().c_str());
        if (dim == 'x' || dim == 'X') {
          has_x = 1;
          point.from_line = line_index + 1;
          point.x_tum = tum;
          point.x_mm = mm;
        } else if (dim == 'y' || dim == 'Y') {
          has_y = 1;
          point.from_line = line_index + 1;
          point.y_tum = tum;
          point.y_mm = mm;
        } else if (dim == 'z' || dim == 'Z') {
          has_z = 1;
          point.from_line = line_index + 1;
          point.z_tum = tum;
          point.z_mm = mm;
        }
      }

      ++line_index;
    }

    ++c;
  }

  s.point_count = point_array_use;
  s.point_arr = (struct point*)malloc(s.point_count * sizeof(struct point));
  for (int k = 0; k < s.point_count; ++k) {
    s.point_arr[k] = point_array[k];
  }

  return s;
}

double map_lin_f(double out_lo, double out_hi, double in_lo, double in_hi, double in, double fallback) {
  double out_w = out_hi - out_lo;
  double in_w = in_hi - in_lo;
  if (in_w == 0) {
    return fallback;
  }

  return out_lo + ((in - in_lo) * out_w) / in_w;
}

static void atexit_read_from_keyboard() {
  // during program termination require user to write on keyboard
  // before text window disappears
  fprintf(stderr, "when ready press any key to close\n\n");
  getchar();
  fprintf(stderr, "live long and prosper\n");
}

int main(int argc, char *argv[]) {
  char *path;
  struct buf infile_buf;
  struct inspection inspection;
  int i;
  int n;

  atexit(atexit_read_from_keyboard);

  if (2 <= argc) {
    path = argv[1];
  } else {
    fprintf(stderr, "hesseplanum expected a file argument, looking for PID file\n");
    path = (char*)"PID";
  }

  char timebuffer[37];
  struct tm *u = 0;
#if __linux__
  time_t t;
  time(&t);
  u = gmtime(&t);
#else
  __int64 t;
  _time64(&t);
  u = new tm;
  int gmtime_rc = _gmtime64_s(u, &t);
#endif
  snprintf(timebuffer, sizeof(timebuffer) - 1, "%04d%02d%02d%02d%02d%02dutc", u->tm_year + 1900,
           u->tm_mon + 1, u->tm_mday, u->tm_hour, u->tm_min, u->tm_sec);

  printf("# hesseplanum %s {path %s}\n", timebuffer, path);
  buf_initude(&infile_buf);
  if (buf_load_or_warn(&infile_buf, path)) {
    fprintf(stderr, "{path %s} was not accessible as a regular file\n", path);
    return 1;
  }

  fprintf(stderr,
          "This program is free software: you can redistribute it and/or modify\n"
          "it under the terms of the GNU General Public License as published by\n"
          "the Free Software Foundation, either version 3 of the License, or (at\n"
          "your option) any later version.\n");

  fprintf(stderr,
          "This program is distributed in the hope that it will be useful, but\n"
          "WITHOUT ANY WARRANTY; without even the implied warranty of\n"
          "MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU\n"
          "General Public License for more details.\n");

  fprintf(stderr,
          "You should have received a copy of the GNU General Public License along\n"
          "with this program. If not, see <https://www.gnu.org/licenses/>.\n");

  inspection = scan_inspection_from_buffer(&infile_buf);
  fprintf(stderr, "{inspection.point_count %d}\n", (int)(inspection.point_count));

  n = inspection.point_count;
  i = 0;
  while (i < n) {
    struct point *p = inspection.point_arr + i;
    fprintf(stderr, "[%d] {tum %f %f %f} {mm %f %f %f}\n", i, p->x_tum,
            p->y_tum, p->z_tum, p->x_mm, p->y_mm, p->z_mm);
    if (p->index == 2) {
      fprintf(stderr, "# initial three points are interpreted as plane defining\n");
    }
    ++i;
  }

  if (inspection.point_count < 3) {
    fprintf(stderr, "[ERRCODE A3P] expected at least three points, to be able to compute surface, terminating\n");
    return 5;
  }

  double p0x = inspection.point_arr[0].x_mm;
  double p0y = inspection.point_arr[0].y_mm;
  double p0z = inspection.point_arr[0].z_mm;
  double p1x = inspection.point_arr[1].x_mm;
  double p1y = inspection.point_arr[1].y_mm;
  double p1z = inspection.point_arr[1].z_mm;
  double p2x = inspection.point_arr[2].x_mm;
  double p2y = inspection.point_arr[2].y_mm;
  double p2z = inspection.point_arr[2].z_mm;
  double ax = p1x - p0x;
  double ay = p1y - p0y;
  double az = p1z - p0z;
  double amag = sqrt(ax * ax + ay * ay + az * az);
  double const amag_min = 2;
  if (amag < amag_min) {
    fprintf(stderr, "[ERRCODE V1C] expected first and second points {dist %G} to be at least %G mm apart, terminating\n", amag, amag_min);
    return 6;
  }

  double bx = p2x - p0x;
  double by = p2y - p0y;
  double bz = p2z - p0z;
  double bmag = bx * bx + by * by + bz * bz;
  double const bmag_min = 2;
  if (bmag < bmag_min) {
    fprintf(stderr, "[ERRCODE V2C] expected first and third points {dist %G} to be at least %G mm apart, terminating\n", bmag, bmag_min);
    return 7;
  }

  /* hessian plane (unit normal and distance) from three points */
  double a = ay * bz - az * by;
  double b = az * bx - ax * bz;
  double c = ax * by - ay * bx;
  double d = -a * p0x - b * p0y - c * p0z;
  double mag = sqrt(a * a + b * b + c * c);
  if (mag < 1e-9) {
    fprintf(stderr, "[ERRCODE M0P] expected {mag %g} to be further from zero, terminating\n", mag);
    exit(1);
  }
  a /= mag;
  b /= mag;
  c /= mag;
  d /= mag;

  fprintf(stderr, "hessian plane {a %f} {b %f} {c %f} {d %f}\n", (double)(a), (double)(b), (double)(c), (double)(d));
  fprintf(stderr, "# https://mathworld.wolfram.com/HessianNormalForm.html\n");

  struct point* *points_depth_sorted = (struct point**)malloc((inspection.point_count - 3) * sizeof(struct point**));
  int points_depth_sorted_ix = 0;
  struct point *p;
  i = 3;
  n = inspection.point_count;
  while (i < n) {
    p = (struct point*)inspection.point_arr + i;
    p->plane_distance = a * p->x_mm + b * p->y_mm + c * p->z_mm + d;
    points_depth_sorted[points_depth_sorted_ix] = p;
    // fprintf(stderr, "#1 [%d] {p->plane_distance %G}\n", i, p->plane_distance);
    ++i;
    ++points_depth_sorted_ix;
  }

  qsort(points_depth_sorted, points_depth_sorted_ix, sizeof(struct point*), point_compare_plane_distance);

  i = 0;
  n = points_depth_sorted_ix;
  while (i < n) {
    p = (struct point*)points_depth_sorted[i];
    fprintf(stderr, "depth sorted [%d] {p->plane_distance %f} {p->index %d}\n", (int)(i), (double)(p->plane_distance), (int)(p->index));

    ++i;
  }

  int const dist_array_element_count = inspection.point_count - 3;
  double *dist_array = (double*)malloc(dist_array_element_count * sizeof(double));
  double *A = dist_array;
  int N = dist_array_element_count;
  i = 0;
  n = N;
  while (i < n) {
    d = inspection.point_arr[i + 3].plane_distance;
    A[i] = d;
    ++i;
  }

  double lo = 1e37;
  double hi = -1e37;
  i = 0;
  while (i < N) {
    d = A[i];
    if (d < lo) {
      lo = d;
    }
    if (hi < d) {
      hi = d;
    }
    ++i;
  }

  int q2_mid1 = (N - 1) / 2;
  int q2_mid2 = (N + 0) / 2;
  double q2 = (A[q2_mid1] + A[q2_mid2]) / 2.0f;

  int h = (N + 1) / 2;
  int q1_mid1 = (h - 1) / 2;
  int q1_mid2 = (h + 0) / 2;
  double q1 = (A[q1_mid1] + A[q1_mid2]) / 2.0f;

  int q3_mid1 = N - 1 - q1_mid2;
  int q3_mid2 = N - 1 - q1_mid1;
  double q3 = (A[q3_mid1] + A[q3_mid2]) / 2.0f;

  double /* inter quartile range */ iqr = q3 - q1;

  double lim_lo = q1 - 1.5 * iqr;
  double lim_hi = q3 + 1.5 * iqr;

  i = 0;
  while (i < N) {
    d = A[i];
    if (d < lim_lo || lim_hi < d) {
      fprintf(stderr, "outlier [%d] {d %f}\n", (int)(i), (double)(d));
    }
    ++i;
  }

  int screen_lo = 1;
  int screen_hi = 74;
  int q1_ix = (int)map_lin_f(screen_lo, screen_hi, lo, hi, q1, NAN);
  int q2_ix = (int)map_lin_f(screen_lo, screen_hi, lo, hi, q2, NAN);
  int q3_ix = (int)map_lin_f(screen_lo, screen_hi, lo, hi, q3, NAN);
  int zero_ix = (int)map_lin_f(screen_lo, screen_hi, lo, hi, 0, NAN);

  char Blo[20];
  int BloN = sizeof(Blo) - 1;
  Blo[BloN] = 0;
  snprintf(Blo, BloN, "%g mm", lo);
  int BloW = (int)strlen(Blo);

  char Bhi[20];
  int BhiN = sizeof(Bhi) - 1;
  Bhi[BhiN] = 0;
  snprintf(Bhi, BhiN, "%g mm", hi);
  int BhiW = (int)strlen(Bhi);

  char Bq1[20];
  int Bq1N = sizeof(Bq1) - 1;
  Bq1[Bq1N] = 0;
  snprintf(Bq1, Bq1N, "%g mm", q1);
  int Bq1W = (int)strlen(Bq1);

  char Bq2[20];
  int Bq2N = sizeof(Bq2) - 1;
  Bq2[Bq2N] = 0;
  snprintf(Bq2, Bq2N, "%g mm", q2);
  int Bq2W = (int)strlen(Bq2);

  char Bq3[20];
  int Bq3N = sizeof(Bq3) - 1;
  Bq3[Bq3N] = 0;
  snprintf(Bq3, Bq3N, "%g mm", q3);
  int Bq3W = (int)strlen(Bq3);

  fprintf(stderr, "\n");

  int x;
  int y = 0;
  while (y < 16) {
    x = 0;
    while (x < screen_hi) {
      if (y == 0) {
        if (x == screen_lo) {
          printf("%s", Blo);
          x += BloW;
          continue;
        }

        if (x == screen_hi - BhiW) {
          printf("%s", Bhi);
          x += BhiW;
          continue;
        }
      }

      if (y == 1) {
        if (x == q2_ix - Bq2W / 2) {
          printf("%s", Bq2);
          x += Bq2W;
          continue;
        }
      }

      if (y == 2) {
        if (x == screen_lo) {
          printf("l");
          x += 1;
          continue;
        }

        if (x == q2_ix) {
          printf("q");
          x += 1;
          continue;
        }

        if (x == screen_hi - 1) {
          printf("h");
          x += 1;
          continue;
        }
      }

      if (y == 3) {
        if (x == screen_lo) {
          printf("o");
          x += 1;
          continue;
        }
        if (x == q2_ix) {
          printf("2");
          x += 1;
          continue;
        }

        if (x == screen_hi - 1) {
          printf("i");
          x += 1;
          continue;
        }
      }

      if (y == 5 || y == 9) {
        if (q1_ix < x && x < q3_ix) {
          printf("*");
          x += 1;
          continue;
        }
      }

      if (y == 7) {
        if (screen_lo < x && x < q1_ix) {
          printf("-");
          x += 1;
          continue;
        }

        if (q3_ix < x && x < screen_hi - 1) {
          printf("-");
          x += 1;
          continue;
        }
      }

      if (6 <= y && y <= 8) {
        if (x == screen_lo || x == screen_hi - 1) {
          printf("#");
          x += 1;
          continue;
        }

        if (x == q1_ix || x == q3_ix) {
          printf("*");
          x += 1;
          continue;
        }

        if (x == q2_ix) {
          printf("|");
          x += 1;
          continue;
        }
      }

      if (y == 11) {
        if (x == q1_ix) {
          printf("q");
          x += 1;
          continue;
        }

        if (x == q3_ix) {
          printf("q");
          x += 1;
          continue;
        }
      }

      if (y == 12) {
        if (x == q1_ix) {
          printf("1");
          x += 1;
          continue;
        }

        if (x == q3_ix) {
          printf("3");
          x += 1;
          continue;
        }
      }

      if (y == 14) {
        if (x == q1_ix - Bq1W / 2) {
          printf("%s", Bq1);
          x += Bq1W;
          continue;
        }

        if (x == q3_ix - Bq3W / 2) {
          printf("%s", Bq3);
          x += Bq3W;
          continue;
        }
      }

      if (x == zero_ix) {
        printf("0");
      } else {
        printf(" ");
      }
      ++x;
    }
    printf("\n");

    ++y;
  }

  printf("q1 to q3             %10G mm\n", q3 - q1);
  printf("thickness min to max %10G mm\n", hi - lo);
  fprintf(stderr, "\ndone, ");

  return 0;
}
