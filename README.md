# hesseplanum

box plot plane projected points to get a feel about how plane or squiggly a surface is

# program input
point data is read from a text file provided as argument, or the program looks for a neighbour file named PID

initial three points are interpreted as plane defining

```
(defun hesse-plane-from-three-points(p0x p0y p0z p1x p1y p1z p2x p2y p2z)
  (setq ax (- p1x p0x)
        ay (- p1y p0y)
        az (- p1z p0z)
        bx (- p2x p0x)
        by (- p2y p0y)
        bz (- p2z p0z)
        a (- (* ay bz) (* by az))
        b (- (* bx az) (* ax bz))
        c (- (* ax by) (* ay bx))
        d (- 0 (* a p0x) (* b p0y) (* c p0z))
        mag (sqrt (+ (* a a) (* b b) (* c c)))
        a (/ a mag)
        b (/ b mag)
        c (/ c mag)
        d (/ d mag))
  (format "{nx %s} {ny %s} {nz %s} {d %s}"
          a b c d))

(hesse-plane-from-three-points
 1  7  -3

 -5  -5  -3

 6  -7  -3
 ) ;; "{nx 0.0} {ny 0.0} {nz 1.0} {d 3.0}"
```

following points are distance measured to the plane


# program output
example output from test data, a text graphics course grained box plot

```
 -0.238821 mm                                               -0.00682724 mm
                  -0.163341 mm
 l                      q                                                h
 o                      2                                                i

                     **************************
 #                  *   |                      *                         #
 #------------------*   |                      *-------------------------#
 #                  *   |                      *                         #
                     **************************

                    q                          q
                    1                          3

              -0.175276 mm               -0.0920365 mm

q1 to q3              0.0832394 mm
thickness min to max   0.231994 mm
```
